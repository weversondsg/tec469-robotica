#pragma config(Sensor, S4, sonarSensor, sensorSONAR)
#pragma config(Sensor, S3, rightLightSensor, sensorLightActive)
#pragma config(Sensor, S2, leftLightSensor, sensorLightActive)
/*---------------------------------
  Consts
  ---------------------------------*/
const int STOPPED = 0;
const int FOLLOWING_LINE = 1;
const int TURNING_LEFT = 2;
const int TURNING_RIGHT = 3;
/*---------------------------------
  Global Variables
  ---------------------------------*/
int valueOfBlack = 41;
int valueOfWhite = 69;
int valueOfSilver = 87;
int robotVelocity = 35;
int black = 0, white = 1, silver = 2;
//const TMailboxIDs entrada1 = mailbox1;
int state = STOPPED;
int rotation = 0;
int xPosition = 0, yPosition = 0;
/*---------------------------------
  Function prototypes
  ---------------------------------*/
void avoidObstacle();
void calibrateLightSensor();
int readColorWithRightLightSensor();
int readColorWithLeftLightSensor();
void moveForward(int centimeters, int velocity);
float moveForwardFollowingLine(int velocity);
void rotateToRight(int velocity, int degrees);
int rotateToLeft(int velocity, int degrees);
int rotateToRightUntilFindTheLine(int velocity);
int rotateToLeftUntilFindTheLine(int velocity);
task receiveBluetoothMessage();
void awaitForBTMessage();
void sendPosition();
void followLine();
/*---------------------------------
  Functions
  ---------------------------------*/
void calibrateLightSensor()
{
  wait1Msec(1000);
	while(nNxtButtonPressed != 3)
	{
		valueOfBlack = SensorValue(leftLightSensor);
		nxtDisplayCenteredTextLine(3, "BLACK");
		nxtDisplayCenteredTextLine(4, "%d", valueOfBlack);
		wait1Msec(1);
	}
	nxtDisplayCenteredTextLine(3, "DEFINED");
  wait1Msec(1500);

	while(nNxtButtonPressed != 3)
	{
		valueOfWhite = SensorValue(leftLightSensor);
		nxtDisplayCenteredTextLine(3, "WHITE");
		nxtDisplayCenteredTextLine(4, "%d", valueOfWhite);
		wait1Msec(1);
	}
	nxtDisplayCenteredTextLine(3, "DEFINED");
  wait1Msec(1500);

	while(nNxtButtonPressed != 3)
	{
		valueOfSilver = SensorValue(leftLightSensor);
		nxtDisplayCenteredTextLine(3, "SILVER");
		nxtDisplayCenteredTextLine(4, "%d", valueOfSilver);
		wait1Msec(1);
	}
	nxtDisplayCenteredTextLine(3, "READY!");
  wait1Msec(3000);
}

int readColorWithRightLightSensor() {
  int value = SensorValue(rightLightSensor);
  /*int value1 = abs(valueOfBlack - value);
  int value2 = abs(valueOfWhite - value);
  int value3 = abs(valueOfSilver - value);*/
  int result = 0;

  /*if (value1 <= value2 && value1 <= value3) {
    result = black;
  } else if (value2 <= value1 && value2 <= value3) {
    result = white;
  } else {
    result = silver;
  }*/
  if (value < 45) {
    result = black;
  } else if (value > 80) {
    result = silver;
  } else {
    result = white;
  }
  return result;
}

int readColorWithLeftLightSensor() {
  int value = SensorValue(leftLightSensor);
  /*int value1 = abs(valueOfBlack - value);
  int value2 = abs(valueOfWhite - value);
  int value3 = abs(valueOfSilver - value);*/
  int result = 0;

  /*if (value1 <= value2 && value1 <= value3) {
    result = black;
  } else if (value2 <= value1 && value2 <= value3) {
    result = white;
  } else {
    result = silver;
  }*/
  if (value < 45) {
    result = black;
  } else if (value > 80) {
    result = silver;
  } else {
    result = white;
  }
  return result;
}

void moveForward(int centimeters, int velocity) {
  int goal = 20.473 * centimeters;

	nMotorEncoder[motorB] = 0;
	nMotorEncoder[motorC] = 0;

	nMotorEncoderTarget[motorB] = goal;

	while(nMotorEncoder[motorB] < goal) {
		motor[motorB] = velocity;
		motor[motorC] = velocity;
  }
  motor[motorB] = 0;
	motor[motorC] = 0;
}

void moveForwardUntilFindTheLine(int velocity) {
	while(readColorWithLeftLightSensor() == white && readColorWithRightLightSensor() == white) {
		motor[motorB] = velocity;
		motor[motorC] = velocity;
  }
  motor[motorB] = 0;
	motor[motorC] = 0;
}

float moveForwardFollowingLine(int velocity) {
	nMotorEncoder[motorB] = 0;
	nMotorEncoder[motorC] = 0;

	motor[motorB] = velocity;
	motor[motorC] = velocity;

	while(readColorWithRightLightSensor() == white && readColorWithLeftLightSensor() == white) {
    wait1Msec(10);
    if(SensorValue[sonarSensor] < 7){
      avoidObstacle();
    }
  }

  int distance = ((nMotorEncoder[motorB] + nMotorEncoder[motorC]) / 2) / 20.473;
  return distance;
}

void rotateToRight(int velocity, int degrees) {
  //20.473 * 0.148 = 3.03
  int goal = 3 * degrees;

	nMotorEncoder[motorB] = 0;
	nMotorEncoder[motorC] = 0;

	nMotorEncoderTarget[motorB] = goal;

	while(nMotorEncoder[motorB] < goal) {
		motor[motorB] = velocity;
		motor[motorC] = -velocity;
  }
  motor[motorB] = 0;
	motor[motorC] = 0;
}

int rotateToLeft(int velocity, int degrees) {
  //20.473 * 0.148 = 3.03
  int goal = 3 * degrees;

	nMotorEncoder[motorB] = 0;
	nMotorEncoder[motorC] = 0;

	nMotorEncoderTarget[motorC] = goal;

	while(nMotorEncoder[motorC] < goal) {
		motor[motorB] = -velocity;
		motor[motorC] = velocity;
  }
  motor[motorB] = 0;
	motor[motorC] = 0;

	int angle = nMotorEncoder[motorC] / 3;
  return angle;
}

int rotateToRightUntilFindTheLine(int velocity) {
	nMotorEncoder[motorB] = 0;
	nMotorEncoder[motorC] = 0;

	motor[motorB] = velocity;
	motor[motorC] = -velocity / 2;

	while(readColorWithRightLightSensor() == black) {
    wait1Msec(1);
  }
  // 20.473 / 0.148 = 138.33
  int angle = ((nMotorEncoder[motorB] - nMotorEncoder[motorC]) / 2) / 3;
  return angle;
}

int rotateToLeftUntilFindTheLine(int velocity) {
	nMotorEncoder[motorB] = 0;
	nMotorEncoder[motorC] = 0;

  motor[motorB] = -velocity / 2;
	motor[motorC] = velocity;

  while(readColorWithLeftLightSensor() == black) {
	  wait1Msec(1);
	  if(readColorWithRightLightSensor() == black && readColorWithLeftLightSensor() == black) {
		    rotation -= rotateToRightUntilFindTheLine(robotVelocity);
		    sendPosition();
		    break;
		}
	}
  int angle = ((nMotorEncoder[motorC] - nMotorEncoder[motorB]) / 2) / 3;
  return angle;
}
/*
void detectDifferential() {
  nxtDisplayCenteredTextLine(3, "SEARCHING");
  nxtDisplayCenteredTextLine(4, "DIFFERENTIAL...");
  while(readColorWithRightLightSensor() != silver) {
    	motor[motorB] = 10;
	    motor[motorC] = 10;
  }
  nxtDisplayCenteredTextLine(3, "SILVER COLOR");
  nxtDisplayCenteredTextLine(4, "DETECTED");

	while(nNxtButtonPressed != 3)
	{
		nxtDisplayCenteredTextLine(3, "AWAITING COMMAND");
		nxtDisplayCenteredTextLine(4, "TO START");
		wait1Msec(10);
	}
}*/

void avoidObstacle() {
  rotateToLeft(robotVelocity,90);

  ubyte myMsg[1]; // send a message
	myMsg[0] = 251;
	nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);

  int distance = ((nMotorEncoder[motorB] + nMotorEncoder[motorC]) / 2) / 20.473;
	wait1Msec(20);

	nMotorEncoder[motorB] = 0;
	nMotorEncoder[motorC] = 0;

	if(SensorValue[sonarSensor] < 26) { // desvia pela direita
		rotateToRight(robotVelocity,180);
		moveForward(25,robotVelocity);
		rotateToLeft(robotVelocity,90);
		moveForward(50,robotVelocity);
    rotateToLeft(robotVelocity,90);
		moveForwardUntilFindTheLine(robotVelocity);
		moveForward(5,robotVelocity);
    rotateToRightUntilFindTheLine(robotVelocity);
	} else {// desvia pela esquerda
	  moveForward(25,robotVelocity);
	  rotateToRight(robotVelocity,90);
	  moveForward(50,robotVelocity);
	  rotateToRight(robotVelocity,90);
		moveForwardUntilFindTheLine(robotVelocity);
		moveForward(5,robotVelocity);
	  rotateToLeftUntilFindTheLine(robotVelocity);
  }
}

task receiveBluetoothMessage() {
  setBluetoothRawDataMode();
  while (!bBTRawMode) {
    nxtDisplayCenteredTextLine(4, "Inside RAW");
		wait1Msec(1);
  }
	nxtDisplayCenteredTextLine(4, "After RAW");
  while(true) {
    awaitForBTMessage();
    wait1Msec(1);
  }
}

void awaitForBTMessage() {
  ubyte bufferEntrada[1];                                       // create a ubyte array, 'BytesRead' of size 'bufferSize'.
  int nNumbBytesRead;
  nNumbBytesRead = nxtReadRawBluetooth(&bufferEntrada[0], 1);   // store 'bufferSize' amount of bytes into 'BytesRead[0]'.
  if(bufferEntrada[0] == 'c') {
    nxtDisplayCenteredTextLine(5, "MSG %c", bufferEntrada[0]);
    bufferEntrada[0] = 0;
	  state = FOLLOWING_LINE;
	  ubyte myMsg[1]; // Byte array to send
	  myMsg[0] = 252;
	  nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
  } else if(bufferEntrada[0] == 'p') {
    nxtDisplayCenteredTextLine(5, "MSG %c", bufferEntrada[0]);
	  state = STOPPED;
	  bufferEntrada[0] = 0;
  } else if(bufferEntrada[0] == 'a') {
    nxtDisplayCenteredTextLine(5, "MSG %c", bufferEntrada[0]);
	  bufferEntrada[0] = 0;
	  rotation += rotateToLeft(robotVelocity, 90);
	  ubyte myMsg[1]; // Byte array to send
	  myMsg[0] = rotation;
	  nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
  }
}

void stopRobot() {
  motor[motorB] = 0;
	motor[motorC] = 0;
}

void sendPosition() {
  /* Protocol
  255 = position
  254 = negative
  253 = positive
  252 = following the line
  251 = obstacle
  */
  setBluetoothRawDataMode();
  ubyte myMsg[1];
	myMsg[0] = (255);
	nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);

	if(xPosition < 0) {
	  myMsg[0] = 254;
	  nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
  } else {
    myMsg[0] = 253;
	  nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
  }
	myMsg[0] = abs(xPosition);
	nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);

	if(yPosition < 0) {
	  myMsg[0] = 254;
	  nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
  } else {
    myMsg[0] = 253;
	  nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
  }
	myMsg[0] = abs(yPosition);
	nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
}

void followLine() {
  int distance = 0;
  while(true) {
    if(state == FOLLOWING_LINE) {
      nxtDisplayCenteredTextLine(3, "FL");
	  	if(readColorWithRightLightSensor() == white && readColorWithLeftLightSensor() == white) {
		    ubyte myMsg[1];
      	myMsg[0] = 252;
	      nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
		    distance = moveForwardFollowingLine(robotVelocity);
		    xPosition += distance * cosDegrees(rotation);
		    yPosition += distance * sinDegrees(rotation);
		  } else if(readColorWithRightLightSensor() == black && readColorWithLeftLightSensor() == white) {
		    rotation -= rotateToRightUntilFindTheLine(robotVelocity);
		    sendPosition();
		  } else if(readColorWithRightLightSensor() == white && readColorWithLeftLightSensor() == black) {
		    rotation += rotateToLeftUntilFindTheLine(robotVelocity);
		    sendPosition();
		  } else if(readColorWithRightLightSensor() == black && readColorWithLeftLightSensor() == black) {
		    rotation -= rotateToRightUntilFindTheLine(robotVelocity);
		    sendPosition();
		  } else {
		    ubyte myMsg[1];
      	myMsg[0] = 250;
	      nxtWriteRawBluetooth(nBTCurrentStreamIndex , myMsg, 1);
		    stopRobot();
		  }
		} else {
      nxtDisplayCenteredTextLine(3, "STOPPED");
		  stopRobot();
		}
		sendPosition();
	}
	wait1Msec(10);
}

task main()
{
	//calibrateLightSensor();
  //detectDifferential();
	//ClearMessage();
  StartTask(receiveBluetoothMessage, 10);
  followLine();
}
