import processing.serial.*;

Serial serial;
boolean isNegative = false;
ArrayList<Edge> path;
int value = 0;
String textStatus;
int oldX = 10, oldY = 200;

void setup() {
  serial = new Serial(this, Serial.list()[1], 9600);
  path = new ArrayList<Edge>();
  size(600, 600);
  println("Connected");
  textStatus = "Robô está na base de recarga";
}

int sequence = 0;
int newX = 0, newY = 0;
void draw() {
  if (serial.available() > 0) {
    int inByte = serial.read();
    if(inByte == 255) {
      println("New position");
    } else if(inByte == 254) {
      print("-");
    } else if(inByte == 253) {
      print("+");
    } else if(inByte == 252) {
      textStatus = "Robô está percorrendo o circuito";
    } else if(inByte == 251) {
      textStatus = "Desviando de obstáculo";
      path.add(new Edge(oldX, oldY, oldX + 10, oldY));
      path.add(new Edge(oldX + 10, oldY, oldX + 10, oldY + 10));
      path.add(new Edge(oldX + 10, oldY + 10, oldX, oldY + 10));
      path.add(new Edge(oldX, oldY + 10, oldX, oldY));
    } else if(inByte == 250) {
      textStatus = "Robô está na base de recarga";
    } else {
      if(sequence == 0) {
        newX = inByte + 10;
        sequence = 1;
        println("X: " + inByte);
      } else {
        newY = (inByte * (-1)) + 200;
        sequence = 0;
        println("Y: " + inByte);
        path.add(new Edge(oldX, oldY, newX, newY));
        oldX = newX;
        oldY = newY;
      }
    }
  }
  background(255);
  drawTexts();
  drawPath();
}

void keyPressed() {
  if (key=='c') {
    println("sending c");
    serial.write('c');
  }
 
  if (key=='p') {
    println("sending p");
    serial.write('p');
   }
 
  if (key=='a') {
    println("sending a");
    serial.write('a');
   }
 
   if (key=='q') {
    serial.clear();
    serial.stop();
    println("CLOSING PORT");
   }  
}

void drawPath() {
  for(int i = 0; i < path.size(); i++) {
    Edge auxEdge = path.get(i);
    line(auxEdge.x1, auxEdge.y1, auxEdge.x2, auxEdge.y2);
  }
}

void drawTexts() {
  fill(0, 102, 153);
  text("Status: " + textStatus, 10, 30);
}

class Edge {
  int x1, y1;
  int x2, y2;
  
  Edge(int x1, int y1, int x2, int y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }
}
